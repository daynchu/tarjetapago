import React, { useState } from "react";

const Flipped = ({ card }) => {
  const [flip, setFlip] = useState(false);

  const { cardMask, cardNumber, cardHolder } = card;

  let re = /(\w+)\s(\w+)/;
  let names = cardHolder.replace(re, "$1, $2");

  const showName = names.length === 0;
  const nameStye = showName ? "c-holder px-1" : "c-holder px-1";


  const flipped = flip ? "card flipped" : "card";
  return (
    <>
      <div className="card-main">
        <div className={flipped}>
          <div className="front shadow-xl p-4">
            {" "}
            <div>
              <div className="flex justify-between p-2">
                <img
                  className="w-12"
                  src="https://raw.githubusercontent.com/muhammederdem/credit-card-form/master/src/assets/images/chip.png"
                  alt="chip"
                />
                <div>
                  <img src="/image/mc_symbol_opt_45_1x.png" alt="master" />
                </div>
              </div>
              <div className="mt-4 flex text-white justify-around p-2">
                <span>{cardMask[0]}</span>
                
              </div>
              <div className="flex mt-2 px-1 justify-between">
                <div className={nameStye}>
                  <span className="text-sm text-gray-400">Card Holder</span>
                  <span className="block text-white uppercase ">
                    {showName
                      ? "FULL NAMES"
                      : names
                          .split("")
                          .map(each => (
                            <span className="animate__animated animate__fadeInLeft animate__slower	3s">
                              {each}
                            </span>
                          ))}
                  </span>
                </div>
                <div className="text-center">
                  <span className="text-sm text-gray-400">Expiración</span>
                  <span className="block text-white">
                    <span>MM</span> / <span>YY</span>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div className="back">
            {" "}
            <button onClick={() => setFlip(prevState => !prevState)}>
              Go Back
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Flipped;
