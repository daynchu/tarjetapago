import React, { useEffect, useState } from "react";
import Cards from "react-credit-cards";
import "react-credit-cards/es/styles-compiled.css";
import { Button, TextField, Container, Grid } from '@material-ui/core';
import "./form.scss"
import logo from "../assets/logodata.png"
import axios from "axios";
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import logoheader from "../assets/tarjetadata-sanjuan.png"
import BackGorundHeader from "../assets/background.jpg"
import checkSuccess from "../assets/check.png"
import Impresora from "../assets/printer.png"
import Avatar from '@material-ui/core/Avatar';
import Swal from 'sweetalert2'
import pdf from "../assets/terminos_y_condiciones2022.pdf"


const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    root: {
        display: 'flex',
        '& > * + *': {
            marginLeft: theme.spacing(2),
        },
    },
}));


const Form = () => {
    // var servidor = "http://" + window.location.hostname+":3800"
    var servidor = "https://pagosexternos.tarjetadata.com.ar/"
    const [user, setUser] = useState({ name: "", number: "", expiry: "", focus: "", plan: "", tipoDoc: "", NumDoc: "", selCuotas: "",email: "" });
    const [token, setToken] = useState(null);
    const [pagoValido, setPagoValido] = useState(false);
    const [infoLink, setInfoLink] = useState(null);
    const [loading, setLoading] = useState(false);
    const [planPago, setPlanes] = useState(null)
    const [cuotas, setCuotas] = useState(null)
    const [cheked, SetCheked] = useState(false)
    const [ValorCuota, setValorCuota] = useState(null)
    const [PagoSuccess, setPagoSuccess] = useState(null)
    const [CallbackExito, setCallbackExito] = useState(null)
    const DatosLink=servidor+"/Comercio/Comercio/infoLink/GetLink"
    const Planes= servidor+"/Comercio/Comercio/serviciosLinkPagos/Planes"
    const ProcesarPagos= servidor+"/Comercio/Comercio/serviciosLinkPagos/procesarPago"
    const CalcularValorCuota= servidor+"/Comercio/Comercio/serviciosLinkPagos/montoCuota"

    // const DatosLink=servidor+"/Comercio/infoLink/GetLink"
    // const Planes= servidor+"/Comercio/serviciosLinkPagos/Planes"
    // const ProcesarPagos= servidor+"/Comercio/serviciosLinkPagos/procesarPago"
    // const CalcularValorCuota= servidor+"/Comercio/serviciosLinkPagos/montoCuota"

    // const DatosLink = process.env.REACT_APP_API_URL + "/infoLink/GetLink"
    // const Planes = process.env.REACT_APP_API_URL + "/serviciosLinkPagos/Planes"
    // const ProcesarPagos = process.env.REACT_APP_API_URL + "/serviciosLinkPagos/procesarPago"

    // const DatosLink=process.env.REACT_APP_API2+"/infoLink/GetLink"
    // const Planes= process.env.REACT_APP_API2+"/serviciosLinkPagos/Planes"
    // const ProcesarPagos= process.env.REACT_APP_API2+"/serviciosLinkPagos/procesarPago"
    const classes = useStyles();
    const numberFormat2 = new Intl.NumberFormat('es-AR');

    useEffect(() => {
        getToken()
        if (token !== null) {
            getDatos()
        }
    }, [token]);

    useEffect(() => {
        if (infoLink !== null) {
            if (infoLink.Lote.plan === null) {
                traerPlanes()
            } else {
                setLoading(true);
            }
        }

    }, [infoLink]);



    useEffect(() => {
        if (planPago !== null)
            setLoading(true);
    }, [planPago]);


    useEffect(() => {
        if (user.plan !== "")
            cargarCuotas()
    }, [user.plan]);



    useEffect(() => {
        if (user.selCuotas !== "")
            traerInteres()
    }, [user.selCuotas]);


    const traerInteres = () => 
    {
        // DAVID
        console.log("entro")
        console.log(user.selCuotas)

        // http://localhost:8050/data/autorizadorweb/calcularCuota?plan=5&cuota=2&valor=100.10
        axios({
            method: 'get',
            url: CalcularValorCuota,
            headers: { "Authorization": `Bearer ${token}` },
            params: { plan: user.plan,cuota:user.selCuotas,valor:infoLink.Lote.monto}
        }).then((response) => {
            setValorCuota(response.data.montocuota)
            // setPlanes(response.data)
        });

    }


    const cargarCuotas = () => {
        planPago.Planes.forEach(e => {
            if (e.id === user.plan) {
                setCuotas(e.cuotas)
            }
        });
    }


    const traerPlanes = () => {

        axios({
            method: 'get',
            url: Planes,
            headers: { "Authorization": `Bearer ${token}` },
            params: { IDComercio: infoLink.Lote.nroComercio }
        }).then((response) => {
            console.log(response.data)
            setPlanes(response.data)
        });


    }

    const getDatos = () => {
        axios({
            method: 'get',
            url: DatosLink,
            headers: { "Authorization": `Bearer ${token}` },
        }).then((response) => {
            // console.log(response)
            if (response.data.status === 401) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: "Token invalido o vencido",
                    allowOutsideClick: false,
                    showConfirmButton: false
                })
            } else {
                setInfoLink(response.data)
                console.log(response.data)
                if(response.data.Lote.email !== null)
                {
                    user.email=response.data.Lote.email
                }
                if (response.data.Lote.callbackExito !== "")
                    setCallbackExito(response.data.Lote.callbackExito)
            }
        });

    }

    const getToken = () => {
        const query = window.location.search.substring(1)
        setToken((query.split("="))[1])
    }

    const handleInput = e => {
        let { name, value } = e.target;

        setUser({ ...user, [name]: value });
    };

    const handleInputFocus = e => {
        setUser({
            ...user,
            focus: e.target.name
        });
    };

    const fechaActual = () => {
        let date_ob = new Date();
        let date = ("0" + date_ob.getDate()).slice(-2);
        let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
        let year = date_ob.getFullYear();
        let hours = date_ob.getHours();
        let minutes = date_ob.getMinutes();
        let seconds = date_ob.getSeconds();
        var fecha = year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds
        return fecha
    }

    const submitForm = () => {

        var datosPago = {
            documento: user.NumDoc,
            plastico: user.number,
            vtoplastico: user.expiry,
            cuitcomercio: "",
            empresa: infoLink.Lote.nroComercio,
            total: infoLink.Lote.monto,
            planvta: null,
            cuotasvta: null,
            puntovta: 1,
            email: user.email
        }
        if (infoLink.Lote.plan === "") {
            datosPago.planvta = user.plan
            datosPago.cuotasvta = user.selCuotas
        } else {
            datosPago.planvta = infoLink.Lote.plan
            datosPago.cuotasvta = infoLink.Lote.cuota
        }

        // var pagar ={
        //     "documento":24948718
        //     ,"plastico":"5047880487890062"
        //     ,"vtoplastico":"0921"
        //     ,"cuitcomercio":""
        //     ,"empresa":5555
        //     ,"puntovta":1
        //     ,"total":10.00
        //     ,"planvta":53
        //     ,"cuotasvta":1
        //     }

        axios({
            method: 'post',
            url: ProcesarPagos,
            headers: { "Authorization": `Bearer ${token}` },
            data: datosPago
        }).then((response) => {
            console.log(response)
            if (response.status === 200) {
                if (response.data.hasOwnProperty("DAVentaResponse")) {
                    setPagoSuccess(response.data.DAVentaResponse)
                    setPagoValido(true)
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: response.data.data.Exception,
                    })
                }
            }
        }).catch(error => console.log(error))

        console.log(user)
    };

    const imprimir = (divprint) => {

        let printContents = document.getElementById(divprint).innerHTML;
        let originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }

    return (


        <div>


            {pagoValido ?

                <div id="print" style={{ background: "white", minWidth: "100vh", minHeight: "100vh" }}>

                    <Container fixed>
                    <div style={{ display: "grid", justifyItems: "center",justifyContent:"center" }}>
                            <div style={{ backgroundImage: `url(${BackGorundHeader})`, textAlign:"center",width:"-webkit-fill-available"}}>
                                <img src={logoheader} height="60"  alt="logoCabecera"/>
                            </div>
                            <div>
                                <img src={checkSuccess}  alt="logoSucces"/>
                            </div>
                            <div>
                                <h4 style={{ color: "#428bca" }}> <strong>Transacción Teminada con Éxito</strong></h4>
                            </div>
                            <div className="row">

                                <div>
                                    <label >Código Autorización:</label>
                                </div>
                                <div >{PagoSuccess.num_autorizacion}</div>

                                <div >
                                    <label >Nro Tikect:</label>
                                </div>
                                <div >{infoLink.Lote.id}				</div>

                                <div >
                                    <label class="control-label">Fecha:</label>
                                </div>
                                <div >  {fechaActual()}			</div>

                                <div >
                                    <label class="control-label">Estado</label>
                                </div>
                                <div >APROBADA				</div>

                                <div >
                                    <h4><strong>Referencia :</strong></h4>
                                </div>
                                <div >
                                    <h4 style={{color: "#428bca"}}><strong>{infoLink.Lote.concepto}</strong></h4>
                                </div>

                                <div >
                                    <h4><strong>Empresa :</strong></h4>
                                </div>
                                <div >
                                    <h4 style={{color: "#428bca"}}><strong>{infoLink.Lote.comercio.nombreComercio}</strong></h4>
                                </div>

                                <div style={{ alignSelf: "center" }}>
                                    <strong>
                                        Monto del Pago:
                                    </strong>
                                </div>
                                <div >
                                    <h4 style={{ color: "#428bca" }}>
                                        <strong>$ {new Intl.NumberFormat("es-ES", {style:"currency", currency:"ARS"}).format(infoLink.Lote.monto)} </strong>
                                    </h4>
                                </div>


                            </div>
                            <div onClick={() => { imprimir("print") }} >
                                <Avatar alt="Remy Sharp" src={Impresora} />

                            </div>
                            <div>
                                <h4 style={{ color: "#777" }}> <strong>¡ Gracias por tu compra !</strong></h4>
                            </div>
                           {CallbackExito !== null ? <div>
                                <Button
                                    variant="contained"
                                    style={{
                                        backgroundColor: "green",
                                        "color": "white"
                                    }}
                                    onClick={() => { window.location.href=CallbackExito}}
                                >volver al sitio
                                </Button>
                            </div> : <div></div>}
                            

                        </div>
                    </Container>

                </div> :


                <div className="container">

                    <form
                        onSubmit={submitForm}
                        className="form"
                    >

                        <div className="card-img">
                            <Cards
                                expiry={user.expiry}
                                focused={user.focus}
                                name={user.name}
                                number={user.number}
                                preview="false"
                            />
                        </div>

                        {loading ? <div>
                            {/* <Container maxWidth="sm" style={{"margin-top": "-15%"}}> */}
                            <div className="containerImage">
                                <div className="min-img">
                                    <img src={infoLink.Lote.comercio.logo}  height="100" alt="Logo" style={{ backgroundColor: "black", borderRadius: "10px" }} />
                                </div>

                            </div>
                            <div>
                                <hr />

                                <p style={{ textAlign: "center", fontSize: "12px" }}> Hola {infoLink.Lote.nombreApellido} esta por pagar $ {numberFormat2.format(infoLink.Lote.monto)} de la compra que realizaste en
                                    <br />{infoLink.Lote.comercio.nombreComercio} </p>
                            </div>

                            <div>
                                <h1 style={{ fontSize: "0.7em" }}>
                                    Concepto de lo pagado: {infoLink.Lote.concepto}</h1>
                                <h1 style={{ fontSize: "0.7em" }}>
                                    Referencia: {infoLink.Lote.referencia}</h1>
                                <hr />
                            </div>


                            <h1 style={{ fontSize: "0.7em", fontWeight: "inherit", textAlign: "center" }}>
                                Datos de tu Tarjeta
                            </h1>
                            <h1 style={{ fontWeight: "inherit", fontFamily: 'Roboto', fontSize: "12px" }}>
                                Completar información de la tarjeta
                            </h1>



                            <div className="input-field">
                                <TextField
                                    name="number"
                                    onChange={handleInput}
                                    onFocus={handleInputFocus}
                                    size="small"
                                    type="number"
                                    label="Num. de tarjeta"
                                    onInput = {(e) =>{
                                        e.target.value = Math.max(0, parseInt(e.target.value) ).toString().slice(0,16)
                                    }}
                                    variant="outlined" 
                                    style={{marginBottom:"0.5em"}}/>
                               

                                <TextField
                                    type="number"
                                    name="expiry"
                                    label="Fecha de Vencimiento"
                                    size="small"
                                    variant="outlined"
                                    onChange={handleInput}
                                    onFocus={handleInputFocus}
                                    onInput = {(e) =>{
                                        e.target.value = e.target.value.toString().slice(0,4)
                                    }} 
                                    style={{marginBottom:"0.5em"}}/>
                            

                                <TextField
                                    name="name"
                                    label="Nom. de la Persona"
                                    size="small"
                                    variant="outlined"
                                    onChange={handleInput}
                                    onFocus={handleInputFocus} 
                                    style={{marginBottom:"0.5em"}}
                                    />

                             { infoLink.Lote.email === null ? 
                             <div>
                                  <TextField
                                    name="email"
                                    label="Ingrese email"
                                    size="small"
                                    variant="outlined"
                                    onChange={handleInput}
                                    onFocus={handleInputFocus} />

                             </div>
                            :
                            <div></div>
                            }

                                <FormControl className={classes.formControl}>
                                    <InputLabel id="demo-simple-select-label">Tipo Documento</InputLabel>
                                    <Select
                                        name="tipoDoc"
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value={user.tipoDoc}
                                        onChange={handleInput}
                                    >
                                        <MenuItem value={0}>D.N.I</MenuItem>

                                    </Select>
                                </FormControl>


                                <TextField
                                    type="number"
                                    size="small"
                                    name="NumDoc"
                                    label="Número de identificación"
                                    variant="outlined"
                                    onChange={handleInput}
                                    onInput = {(e) =>{
                                        e.target.value = Math.max(0, parseInt(e.target.value) ).toString().slice(0,8)
                                    }} 
                                />
                            </div>


                            {infoLink.Lote.plan !== null ? <div></div> : <div>

                                <h1 style={{ fontSize: "0.7em", fontWeight: "inherit", textAlign: "center" }}>
                                    Forma de pago
                                </h1>
                                <Grid container spacing={3}>
                                    <Grid item xs={12} sm={6} >
                                        <FormControl className={classes.formControl}>
                                            <InputLabel id="demo-simple-select-label">Plan de Pago</InputLabel>
                                            <Select
                                                name="plan"
                                                id="planDePago"
                                                value={user.plan}
                                                onChange={handleInput}
                                            >
                                                {planPago.Planes.map((p) => (
                                                    <MenuItem value={p.id}>{p.descripcion}</MenuItem>
                                                ))}

                                            </Select>


                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={12} sm={6} >

                                        {cuotas !== null ? <div>
                                            <FormControl className={classes.formControl}>
                                                <InputLabel id="demo-simple-select-label">Cuotas</InputLabel>
                                                <Select
                                                    name="selCuotas"
                                                    labelId="demo-simple-select-label"
                                                    id="demo-simple-select"
                                                    value={user.selCuotas}
                                                    onChange={handleInput}
                                                >
                                                    {cuotas.map((p) => (
                                                        <MenuItem value={p.cuotapln}>{p.cuotapln}</MenuItem>
                                                    ))}
                                                    {/* <MenuItem value={10}>Ten</MenuItem>
          <MenuItem value={20}>Twenty</MenuItem>
          <MenuItem value={30}>Thirty</MenuItem> */}
                                                </Select>
                                            </FormControl></div> : <div></div>}

                                    </Grid>
                                  
                                </Grid>
                                {ValorCuota !== null &&
                                     <div  >

                            <h3 style={{ color: "#428bca" }}>
                            <strong>{user.selCuotas} CUOTAS DE $ {new Intl.NumberFormat("es-ES", {style:"currency", currency:"ARS"}).format(ValorCuota)} </strong>
                            </h3>
</div> }
                                    
                            </div>}


                            <div style={{ "margin-top": "3%", "margin-bottom": "20px",display:"flex",justifyContent:"center" }}>
                                     <Checkbox  value={cheked} onClick={() => {SetCheked(!cheked)}} color="primary" />
                                     <Button href={window.location.origin+"/static/media/terminos_y_condiciones2022.e40e3ba2.pdf"} color="primary" >Acepta Terminos y condiciones</Button>
                                     </div>

                            <Grid item style={{ marginTop: "3%", marginBottom: "20px" }}>
                                <Button
                                    disabled={!cheked}
                                    variant="contained"
                                    style={cheked ? {
                                        "background-color": "#3D5CFF",
                                        "color": "white"
                                    }: {"background-color": "grey",
                                    "color": "white" }}
                                    onClick={() => { submitForm() }}
                                >
                                    Pagar ${numberFormat2.format(infoLink.Lote.monto)}
                                </Button>
                            </Grid>

                            <div className="min-img">
                                <img src={logo} width="100"  alt="Logo" />
                            </div>
                            <h1 style={{ fontSize: "10px", textAlign: "center" }}> Powered by DATA 2000 S.A.</h1>
                            {/* </Container> */}
                        </div>

                            : <div className={classes.root} style={{ placeContent: "center" }}>

                                <CircularProgress /></div>}


                    </form>





                </div>}











        </div>
    );
};

export default Form;